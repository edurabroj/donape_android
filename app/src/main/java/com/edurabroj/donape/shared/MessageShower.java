package com.edurabroj.donape.shared;

import android.app.Activity;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.ViewGroup;

import com.edurabroj.donape.R;

public class MessageShower {
    private Activity activity;

    public MessageShower(Activity activity) {
        this.activity = activity;
    }

    public void showSnackbarMessage(String message){
        View view = getRootView();
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT)
                .show();
    }

    public void showSnackbarWithRetryButton(String message, View.OnClickListener buttonOnClickListener) {
        View view = getRootView();
        Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE)
                .setAction(activity.getString(R.string.action_refresh_after_load_error), buttonOnClickListener)
                .show();
    }

    private View getRootView() {
        final ViewGroup contentViewGroup = activity.findViewById(android.R.id.content);
        View rootView = null;

        if (contentViewGroup != null)
            rootView = contentViewGroup.getChildAt(0);

        if (rootView == null)
            rootView = activity.getWindow().getDecorView().getRootView();

        return rootView;
    }
}
