package com.edurabroj.donape.shared;

import io.reactivex.disposables.Disposable;

public class Disposer {
    public static void dispose(Disposable disposable) {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }
}
