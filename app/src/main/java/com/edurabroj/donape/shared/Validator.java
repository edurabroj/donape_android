package com.edurabroj.donape.shared;

import java.util.regex.Pattern;

public class Validator {
    private static final String EMAIL_REGEX = "^(?i)[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
    private static final String DNI_REGEX = "^[0-9]{8}$";
    private static final String PHONE_REGEX = "^9[0-9]{8}$";
    private static final String PASSWORD_REGEX = "^.{6,}$";

    public static boolean isInvalidEmail(String email) {
        return !Pattern.matches(EMAIL_REGEX, email);
    }

    public static boolean isInvalidDni(String dni) {
        return !Pattern.matches(DNI_REGEX, dni);
    }

    public static boolean isInvalidPhone(String phone) {
        return !Pattern.matches(PHONE_REGEX, phone);
    }

    public static boolean isNullOrEmptyString(String value) {
        return value == null || value.isEmpty();
    }

    public static boolean isInvalidPassword(String password) {
        return !Pattern.matches(PASSWORD_REGEX, password);
    }
}
