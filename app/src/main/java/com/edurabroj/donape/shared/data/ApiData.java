package com.edurabroj.donape.shared.data;

import com.edurabroj.donape.BuildConfig;

public class ApiData {
    private static final String DEBUG_HOST = "http://192.168.0.9:4000/";
    private static final String RELEASE_HOST = "https://donape.herokuapp.com/";

    public static final String HOST = BuildConfig.DEBUG ? DEBUG_HOST : RELEASE_HOST;
    public static final String GRAPHQL_ENDPOINT = "graphql";
    public static final String REGISTER_ENDPOINT = "register";
}
