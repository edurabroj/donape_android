package com.edurabroj.donape.shared.entidades;

import java.util.ArrayList;
import java.util.List;

public class Donacion {
    private int id;
    private double cantidad;
    private String articulo;
    private String fecha;
    private List<Estado> estados;
    private String tituloPublicacion;

    public Donacion() {
        estados = new ArrayList<>();
    }

    public String getTituloPublicacion() {
        return tituloPublicacion;
    }

    public void setTituloPublicacion(String tituloPublicacion) {
        this.tituloPublicacion = tituloPublicacion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getCantidad() {
        return cantidad;
    }

    public String getArticulo() {
        return articulo;
    }


    public String getFecha() {
        return fecha;
    }

    public Estado getUltimoEstado() {
        return getEstados().size() > 0 ? getEstados().get(0) : new Estado();
    }

    public List<Estado> getEstados() {
        return estados;
    }


    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public void setArticulo(String articulo) {
        this.articulo = articulo;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }


    public void setEstados(List<Estado> estados) {
        this.estados = estados;
    }

    public String getTituloCompleto() {
        return getCantidad() + " " + getArticulo() + " para " + getTituloPublicacion();
    }
}
