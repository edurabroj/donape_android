package com.edurabroj.donape.shared.entidades;

public class Necesidad {
    private int id;
    private double cantidadRequerida;
    private double cantidadRecolectada;
    private double cantidadFaltante;
    private String articulo;
    private String tituloPublicacion;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getCantidadRequerida() {
        return cantidadRequerida;
    }

    public void setCantidadRequerida(double cantidadRequerida) {
        this.cantidadRequerida = cantidadRequerida;
    }

    public double getCantidadRecolectada() {
        return cantidadRecolectada;
    }

    public void setCantidadRecolectada(double cantidadRecolectada) {
        this.cantidadRecolectada = cantidadRecolectada;
    }

    public double getCantidadFaltante() {
        return cantidadFaltante;
    }

    public void setCantidadFaltante(double cantidadFaltante) {
        this.cantidadFaltante = cantidadFaltante;
    }

    public String getArticulo() {
        return articulo;
    }

    public void setArticulo(String articulo) {
        this.articulo = articulo;
    }

    public String getTituloPublicacion() {
        return tituloPublicacion;
    }

    public void setTituloPublicacion(String tituloPublicacion) {
        this.tituloPublicacion = tituloPublicacion;
    }
}
