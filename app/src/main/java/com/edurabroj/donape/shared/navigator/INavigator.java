package com.edurabroj.donape.shared.navigator;

public interface INavigator {
    void navigateClearingStack(Class<?> distination);
}
