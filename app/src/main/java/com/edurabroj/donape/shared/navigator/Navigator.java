package com.edurabroj.donape.shared.navigator;

import android.content.Context;
import android.content.Intent;

public class Navigator implements INavigator{
    private Context context;

    public Navigator(Context context){
        this.context = context;
    }

    @Override
    public void navigateClearingStack(Class<?> distination){
        Intent intent = new Intent(context, distination);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
    }
}
