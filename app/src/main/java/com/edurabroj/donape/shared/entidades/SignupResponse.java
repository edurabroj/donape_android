package com.edurabroj.donape.shared.entidades;

import java.util.ArrayList;
import java.util.List;

public class SignupResponse {
    public boolean success;
    public String token;
    public List<ApiError> errors;

    public SignupResponse() {
        errors = new ArrayList<>();
    }
}
