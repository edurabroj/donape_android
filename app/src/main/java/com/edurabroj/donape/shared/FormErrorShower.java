package com.edurabroj.donape.shared;

import android.widget.EditText;

public class FormErrorShower {
    public static void showError(EditText editText, String errorMessage) {
        editText.setError(errorMessage);
        editText.requestFocus();
    }
}
