package com.edurabroj.donape.http;

import com.apollographql.apollo.ApolloClient;
import com.edurabroj.donape.BuildConfig;
import com.edurabroj.donape.components.login.LoginActivity;
import com.edurabroj.donape.shared.navigator.INavigator;
import com.edurabroj.donape.shared.preferences.IPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.edurabroj.donape.shared.data.ApiData.GRAPHQL_ENDPOINT;
import static com.edurabroj.donape.shared.data.ApiData.HOST;
import static com.edurabroj.donape.shared.data.PreferencesData.TOKEN_KEY;
import static com.edurabroj.donape.shared.data.PreferencesData.TOKEN_PREV;

@Module
public class HttpModule {
    @Provides
    @Singleton
    HttpLoggingInterceptor providesHttpBodyLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

    @Provides
    @Singleton
    ApolloClient providesApolloClient(HttpLoggingInterceptor httpLoggingInterceptor, IPreferences preferences, INavigator navigator) {
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();

        if(BuildConfig.DEBUG){
            okHttpBuilder.addInterceptor(httpLoggingInterceptor);
        }

        addAuthToken(okHttpBuilder, preferences);
        addAuthInterceptor(okHttpBuilder, preferences, navigator);

        OkHttpClient okHttpClient = okHttpBuilder.build();

        return ApolloClient.builder()
                .serverUrl(HOST + GRAPHQL_ENDPOINT)
                .okHttpClient(okHttpClient)
                .build();
    }

    private void addAuthInterceptor(OkHttpClient.Builder okHttpBuilder, IPreferences preferences, INavigator navigator) {
        okHttpBuilder.addInterceptor(chain -> {
            Request request = chain.request();
            Response response = chain.proceed(request);

            checkValidAuthToken(response, preferences, navigator);

            return chain.proceed(request);
        });
    }

    private void addAuthToken(OkHttpClient.Builder okHttpBuilder, IPreferences preferences) {
        okHttpBuilder.addInterceptor(chain -> {
            Request.Builder builder = chain.request().newBuilder();

            String authorizationHeaderName = "Authorization";
            String authorizationHeaderValue = TOKEN_PREV + preferences.getStringPreference(TOKEN_KEY);
            builder.addHeader(authorizationHeaderName, authorizationHeaderValue);

            Request request = builder.build();

            return chain.proceed(request);
        });
    }

    private void checkValidAuthToken(Response response, IPreferences preferences, INavigator navigator){
        if(response.code()==401){
            preferences.removeStringPreference(TOKEN_KEY);
            navigator.navigateClearingStack(LoginActivity.class);
        }
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(HttpLoggingInterceptor interceptor) {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        return new Retrofit.Builder()
                .baseUrl(HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }
}
