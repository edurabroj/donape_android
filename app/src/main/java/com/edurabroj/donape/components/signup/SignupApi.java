package com.edurabroj.donape.components.signup;

import com.edurabroj.donape.shared.entidades.SignupResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

import static com.edurabroj.donape.shared.data.ApiData.REGISTER_ENDPOINT;

public interface SignupApi {
    @POST(REGISTER_ENDPOINT)
    Observable<SignupResponse> signup(@Body SignupForm signupForm);
}
