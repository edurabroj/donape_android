package com.edurabroj.donape.components.donacion.donar;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;

import com.edurabroj.donape.R;
import com.edurabroj.donape.root.DonapeApplication;
import com.edurabroj.donape.shared.MessageShower;
import com.edurabroj.donape.shared.entidades.Necesidad;

import javax.inject.Inject;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.edurabroj.donape.shared.data.ExtrasData.EXTRA_NECESIDAD_ID;
import static com.edurabroj.donape.shared.utils.GuiUtils.getBitmap;
import static com.edurabroj.donape.shared.utils.GuiUtils.showMsg;

public class DonarActivity extends AppCompatActivity implements DonarMVP.View {
    @BindView(R.id.refresh)
    SwipeRefreshLayout refreshLayout;

    @BindView(R.id.tvTitulo)
    TextView tvTitulo;

    @BindView(R.id.etCantidad)
    EditText etCantidad;

    @BindView(R.id.tvArticulo)
    TextView tvArticulo;

    @BindView(R.id.btnDonar)
    CircularProgressButton btnDonar;

    @Inject
    DonarMVP.Presenter presenter;

    MessageShower messageShower;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donar);
        ButterKnife.bind(this);

        ((DonapeApplication) getApplication()).getComponent().inject(this);

        messageShower = new MessageShower(this);

        btnDonar.setOnClickListener(v -> presenter.onGuardarDonacionClicked());
        refreshLayout.setOnRefreshListener(() -> presenter.onRefreshDetalleDonacion());
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.attach(this);
        presenter.solicitarDetalleNecesidad();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.detach();
    }

    @Override
    public int getNecesidadId() {
        Bundle extras = getIntent().getExtras();
        int necesidadId = 0;
        if (extras != null) {
            necesidadId = extras.getInt(EXTRA_NECESIDAD_ID);
        }
        return necesidadId;
    }

    @Override
    public float getCantidadADonar() {
        String quantityAsText = etCantidad.getText().toString();
        return quantityAsText.isEmpty() ? 0 : Float.parseFloat(quantityAsText);
    }

    @Override
    public void mostrarDetalleNecesidad(Necesidad necesidad) {
        tvTitulo.setText(necesidad.getTituloPublicacion());
        tvArticulo.setText(necesidad.getArticulo());
    }

    @Override
    public void mostrarMensajeAlGuardarDonacionCorrectamente() {
        showMsg(this, getString(R.string.message_saved_donation));
        Bitmap checkIcon = getBitmap(this, R.drawable.ic_check_24dp);

        btnDonar.doneLoadingAnimation(R.color.colorAccent, checkIcon);

        returnToNeedsList();
    }

    private void returnToNeedsList(){
        new Handler().postDelayed(super::onBackPressed, 600);
    }

    @Override
    public void showInvalidQuantityMessage() {
        String invalidDonationQuantityErrorMessage = getString(R.string.error_invalid_donation_quantity);
        messageShower.showSnackbarMessage(invalidDonationQuantityErrorMessage);
    }

    @Override
    public void mostrarErrorAlObtenerDetalleNecesidad() {
        String getNeedInfoErrorMessage = getString(R.string.error_get_need_info);
        messageShower.showSnackbarWithRetryButton(getNeedInfoErrorMessage, v -> presenter.solicitarDetalleNecesidad());
    }

    @Override
    public void mostrarErrorAlGuardarDonacion() {
        btnDonar.revertAnimation();

        String saveDonationErrorMessage = getString(R.string.error_save_donation);
        messageShower.showSnackbarMessage(saveDonationErrorMessage);
    }

    @Override
    public void mostrarLoadingDetalleNecesidad() {
        refreshLayout.setRefreshing(true);
    }

    @Override
    public void ocultarLoadingDetalleNecesidad() {
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void mostrarLoadingGuardarDonacion() {
        btnDonar.startAnimation();
    }

    @Override
    public void ocultarLoadingGuardarDonacion() {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        btnDonar.dispose();
    }
}
