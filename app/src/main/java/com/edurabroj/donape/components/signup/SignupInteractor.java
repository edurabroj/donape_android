package com.edurabroj.donape.components.signup;

import com.edurabroj.donape.shared.entidades.SignupResponse;

import io.reactivex.Observable;

public class SignupInteractor implements Signup.Interactor {
    private SignupApi signupApi;

    public SignupInteractor(SignupApi signupApi) {
        this.signupApi = signupApi;
    }

    @Override
    public Observable<SignupResponse> signUp(SignupForm signupForm) {
        return signupApi.signup(signupForm);
    }
}
