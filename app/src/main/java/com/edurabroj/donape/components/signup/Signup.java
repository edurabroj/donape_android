package com.edurabroj.donape.components.signup;

import com.edurabroj.donape.exceptions.InvalidInputException;
import com.edurabroj.donape.mvp.BasePresenter;
import com.edurabroj.donape.shared.entidades.ApiError;
import com.edurabroj.donape.shared.entidades.SignupResponse;

import io.reactivex.Observable;

public interface Signup {
    interface View {
        void showLoading();

        void hideLoading();

        String getEmail();

        String getPassword();

        String getFullname();

        String getDni();

        String getPhone();

//        void showNetworkError();

        void showEmailRequired();

//        void showEmailAlreadyTaken();

        void showEmailInvalid();

        void showPasswordRequired();

        void showInvalidPassword();

        void showDniRequired();

        void showInvalidDni();

        void showPhoneRequired();

        void showInvalidPhone();

        void showFullnameRequired();

        void goToMain();

        void onSuccessSignup(SignupResponse signupResponse);

        void onErrorSignup();

        void showApiError(ApiError error);
    }

    interface Presenter extends BasePresenter<View> {
        void onSignupClick();

        void onBackClick();

        void validateForm(SignupForm signupForm) throws InvalidInputException;
    }

    interface Interactor {
        Observable<SignupResponse> signUp(SignupForm signupForm);
    }
}
