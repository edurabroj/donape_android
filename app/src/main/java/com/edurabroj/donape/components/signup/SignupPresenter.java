package com.edurabroj.donape.components.signup;

import com.edurabroj.donape.exceptions.InvalidInputException;
import com.edurabroj.donape.shared.Disposer;
import com.edurabroj.donape.shared.Validator;
import com.edurabroj.donape.shared.entidades.ApiError;
import com.edurabroj.donape.shared.entidades.SignupResponse;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class SignupPresenter implements Signup.Presenter {
    private Signup.View view;
    private Signup.Interactor interactor;

    private Disposable disposableSignUp;

    public SignupPresenter(Signup.Interactor interactor) {
        this.interactor = interactor;
    }

    @Override
    public void onSignupClick() {
        view.showLoading();

        SignupForm signupForm = new SignupForm();
        signupForm.email = view.getEmail();
        signupForm.nombre = view.getFullname();
        signupForm.celular = view.getPhone();
        signupForm.dni = view.getDni();
        signupForm.password = view.getPassword();
        try {
            validateForm(signupForm);
            disposableSignUp = interactor.signUp(signupForm)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableObserver<SignupResponse>() {
                        @Override
                        public void onNext(SignupResponse signupResponse) {
                            if (signupResponse.success) {
                                view.hideLoading();
                                view.onSuccessSignup(signupResponse);
                            } else {
                                view.hideLoading();
                                for (ApiError error : signupResponse.errors) {
                                    view.showApiError(error);
                                }
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            view.hideLoading();
                            view.onErrorSignup();
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } catch (InvalidInputException ignored) {
            view.hideLoading();
        }
    }

    @Override
    public void validateForm(SignupForm signupForm) throws InvalidInputException {
        validateEmailRequired(signupForm.email);
        validateFullnameRequired(signupForm.nombre);
        validateDniRequired(signupForm.dni);
        validatePhoneRequired(signupForm.celular);
        validatePasswordRequired(signupForm.password);

        validateEmail(signupForm.email);
        validateDni(signupForm.dni);
        validatePhone(signupForm.celular);
        validatePassword(signupForm.password);
    }

    private void validatePassword(String password) throws InvalidInputException {
        if (Validator.isInvalidPassword(password) && view != null) {
            view.showInvalidPassword();
            throw new InvalidInputException();
        }
    }

    private void validatePhone(String phone) throws InvalidInputException {
        if (Validator.isInvalidPhone(phone) && view != null) {
            view.showInvalidPhone();
            throw new InvalidInputException();
        }
    }

    private void validateDni(String dni) throws InvalidInputException {
        if (Validator.isInvalidDni(dni) && view != null) {
            view.showInvalidDni();
            throw new InvalidInputException();
        }
    }

    private void validatePasswordRequired(String password) throws InvalidInputException {
        if (Validator.isNullOrEmptyString(password) && view != null) {
            view.showPasswordRequired();
            throw new InvalidInputException();
        }
    }

    private void validatePhoneRequired(String phone) throws InvalidInputException {
        if (Validator.isNullOrEmptyString(phone) && view != null) {
            view.showPhoneRequired();
            throw new InvalidInputException();
        }
    }

    private void validateDniRequired(String dni) throws InvalidInputException {
        if (Validator.isNullOrEmptyString(dni) && view != null) {
            view.showDniRequired();
            throw new InvalidInputException();
        }
    }

    private void validateEmailRequired(String email) throws InvalidInputException {
        if (Validator.isNullOrEmptyString(email) && view != null) {
            view.showEmailRequired();
            throw new InvalidInputException();
        }
    }

    private void validateEmail(String email) throws InvalidInputException {
        if (Validator.isInvalidEmail(email) && view != null) {
            view.showEmailInvalid();
            throw new InvalidInputException();
        }
    }

    private void validateFullnameRequired(String fullname) throws InvalidInputException {
        if (Validator.isNullOrEmptyString(fullname) && view != null) {
            view.showFullnameRequired();
            throw new InvalidInputException();
        }
    }

    @Override
    public void onBackClick() {

    }

    @Override
    public void attach(Signup.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        Disposer.dispose(disposableSignUp);
    }
}
