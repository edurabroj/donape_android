package com.edurabroj.donape.components.signup;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class ModuleSignup {
    @Provides
    Signup.Presenter providePresenter(Signup.Interactor interactor) {
        return new SignupPresenter(interactor);
    }

    @Provides
    Signup.Interactor provideInteractor(SignupApi signupApi) {
        return new SignupInteractor(signupApi);
    }

    @Provides
    @Singleton
    SignupApi provideSignupApi(Retrofit retrofit) {
        return retrofit.create(SignupApi.class);
    }
}
