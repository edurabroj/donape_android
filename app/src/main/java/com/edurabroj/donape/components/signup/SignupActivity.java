package com.edurabroj.donape.components.signup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.edurabroj.donape.R;
import com.edurabroj.donape.components.publicacion.lista.ListaPublicacionActivity;
import com.edurabroj.donape.root.DonapeApplication;
import com.edurabroj.donape.shared.FormErrorShower;
import com.edurabroj.donape.shared.MessageShower;
import com.edurabroj.donape.shared.entidades.ApiError;
import com.edurabroj.donape.shared.entidades.SignupResponse;
import com.edurabroj.donape.shared.preferences.IPreferences;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.edurabroj.donape.shared.data.PreferencesData.TOKEN_KEY;

public class SignupActivity extends AppCompatActivity implements Signup.View {
    @BindView(R.id.loading)
    ProgressBar loading;

    @BindView(R.id.form)
    ViewGroup form;

    @BindView(R.id.email)
    AutoCompleteTextView email;

    @BindView(R.id.fullname)
    AutoCompleteTextView fullname;

    @BindView(R.id.password)
    AutoCompleteTextView password;

    @BindView(R.id.dni)
    AutoCompleteTextView dni;

    @BindView(R.id.phone)
    AutoCompleteTextView phone;

    @BindView(R.id.btnSignup)
    Button btnSignup;

    private MessageShower messageShower;

    @Inject
    Signup.Presenter presenter;

    @Inject
    IPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        ((DonapeApplication) getApplication()).getComponent().inject(this);
        presenter.attach(this);

        messageShower = new MessageShower(this);
        btnSignup.setOnClickListener(v -> presenter.onSignupClick());
    }

    @Override
    public void showLoading() {
        loading.setVisibility(View.VISIBLE);
        form.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        loading.setVisibility(View.GONE);
        form.setVisibility(View.VISIBLE);
    }

    @Override
    public String getEmail() {
        return email.getText().toString();
    }

    @Override
    public String getPassword() {
        return password.getText().toString();
    }

    @Override
    public String getFullname() {
        return fullname.getText().toString();
    }

    @Override
    public String getDni() {
        return dni.getText().toString();
    }

    @Override
    public String getPhone() {
        return phone.getText().toString();
    }

//    @Override
//    public void showNetworkError() {
//        String message = getString(R.string.error_internet);
//        messageShower.showSnackbarWithRetryButton(message, v -> goToMain());
//    }

    @Override
    public void showEmailRequired() {
        String errorMessage = getString(R.string.error_email_required);
        FormErrorShower.showError(email, errorMessage);
    }

//    @Override
//    public void showEmailAlreadyTaken() {
//        String errorMessage = getString(R.string.error_email_already_taken);
//        FormErrorShower.showError(email, errorMessage);
//    }

    @Override
    public void showEmailInvalid() {
        String errorMessage = getString(R.string.error_email_invalid);
        FormErrorShower.showError(email, errorMessage);
    }

    @Override
    public void showPasswordRequired() {
        String errorMessage = getString(R.string.error_password_required);
        FormErrorShower.showError(password, errorMessage);
    }

    @Override
    public void showInvalidPassword() {
        String errorMessage = getString(R.string.error_invalid_password);
        FormErrorShower.showError(password, errorMessage);
    }

    @Override
    public void showDniRequired() {
        String errorMessage = getString(R.string.error_dni_required);
        FormErrorShower.showError(dni, errorMessage);
    }

    @Override
    public void showInvalidDni() {
        String errorMessage = getString(R.string.error_invalid_dni);
        FormErrorShower.showError(dni, errorMessage);
    }

    @Override
    public void showPhoneRequired() {
        String errorMessage = getString(R.string.error_phone_required);
        FormErrorShower.showError(phone, errorMessage);
    }

    @Override
    public void showFullnameRequired() {
        String errorMessage = getString(R.string.error_fullname_required);
        FormErrorShower.showError(fullname, errorMessage);
    }

    @Override
    public void showInvalidPhone() {
        String errorMessage = getString(R.string.error_invalid_phone);
        FormErrorShower.showError(phone, errorMessage);
    }

    @Override
    public void goToMain() {
        Intent intent = new Intent(this, ListaPublicacionActivity.class);
        startActivity(intent);
    }

    @Override
    public void onSuccessSignup(SignupResponse signupResponse) {
        preferences.setStringPreference(TOKEN_KEY, signupResponse.token);
        goToMain();
    }

    @Override
    public void onErrorSignup() {
        String errorMessage = getString(R.string.error_internet);
        messageShower.showSnackbarWithRetryButton(errorMessage, v -> presenter.onSignupClick());
    }

    @Override
    public void showApiError(ApiError error) {
        EditText editText = form.findViewWithTag(error.path);
        FormErrorShower.showError(editText, error.message);
    }
}
