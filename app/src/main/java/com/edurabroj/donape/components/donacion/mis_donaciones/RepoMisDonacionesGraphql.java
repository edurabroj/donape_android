package com.edurabroj.donape.components.donacion.mis_donaciones;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.rx2.Rx2Apollo;
import com.edurabroj.donape.DonacionesByUsuarioQuery;
import com.edurabroj.donape.shared.entidades.Donacion;
import com.edurabroj.donape.shared.entidades.Estado;
import com.edurabroj.donape.shared.entidades.Imagen;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

public class RepoMisDonacionesGraphql implements RepoMisDonaciones {
    private ApolloClient client;

    RepoMisDonacionesGraphql(ApolloClient client) {
        this.client = client;
    }

    @Override
    public Observable<Donacion> getMyDonationsData() {
        DonacionesByUsuarioQuery query = DonacionesByUsuarioQuery.builder().build();
        ApolloCall<DonacionesByUsuarioQuery.Data> apolloCall = client.query(query);
        return Rx2Apollo.from(apolloCall)
                .toObservable()
                .concatMap((Response<DonacionesByUsuarioQuery.Data> dataResponse) ->
                        Observable.fromIterable(dataResponse.data().donacionesByUsuario())
                )
                .concatMap((Function<DonacionesByUsuarioQuery.DonacionesByUsuario, Observable<Donacion>>) donacionApi -> {
                            Donacion donacion = obtenerDonacionMapeada(donacionApi);
                            return Observable.just(donacion);
                        }
                );
    }

    private Donacion obtenerDonacionMapeada(DonacionesByUsuarioQuery.DonacionesByUsuario donacionApi) {
        Donacion donacion = new Donacion();

        donacion.setId(donacionApi.id());
        donacion.setCantidad(donacionApi.cantidad());
        donacion.setArticulo(donacionApi.articuloDonado());
        donacion.setFecha(donacionApi.fecha().toString());

        String titulo = obtenerTituloDonacion(donacionApi);
        donacion.setTituloPublicacion(titulo);

        List<Estado> estados = obtenerEstadosMapeados(donacionApi);
        donacion.setEstados(estados);

        return donacion;
    }

    private String obtenerTituloDonacion(DonacionesByUsuarioQuery.DonacionesByUsuario donacionApi) {
        if (donacionApi.necesidad() != null && donacionApi.necesidad().publicacion() != null) {
            return donacionApi.necesidad().publicacion().titulo();
        }
        return "";
    }

    private List<Estado> obtenerEstadosMapeados(DonacionesByUsuarioQuery.DonacionesByUsuario donacionApi) {
        List<Estado> estados = new ArrayList<>();

        if (donacionApi.estados() != null) {
            for (DonacionesByUsuarioQuery.Estado estadoApi : donacionApi.estados()) {
                Estado estado = obtenerEstadoMapeado(estadoApi);
                estados.add(estado);
            }
        }

        return estados;
    }

    private Estado obtenerEstadoMapeado(DonacionesByUsuarioQuery.Estado estadoApi) {
        Estado estado = new Estado();

        estado.setNombre(estadoApi.estado() != null ? estadoApi.estado().nombre() : "");
        estado.setFecha(estadoApi.fecha() != null ? estadoApi.fecha().toString() : "");

        List<Imagen> imagenes = obtenerImagenesEstado(estadoApi);
        estado.setImagenes(imagenes);

        return estado;
    }

    private List<Imagen> obtenerImagenesEstado(DonacionesByUsuarioQuery.Estado estadoApi) {
        List<Imagen> imagenes = new ArrayList<>();

        if (estadoApi.imagenes() != null) {
            for (DonacionesByUsuarioQuery.Imagene imagenApi : estadoApi.imagenes()) {
                Imagen imagen = new Imagen();
                imagen.setUrl(imagenApi.url());

                imagenes.add(imagen);
            }
        }

        return imagenes;
    }
}
