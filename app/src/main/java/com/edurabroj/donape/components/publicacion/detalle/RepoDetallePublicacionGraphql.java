package com.edurabroj.donape.components.publicacion.detalle;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.rx2.Rx2Apollo;
import com.edurabroj.donape.PublicacionQuery;
import com.edurabroj.donape.shared.entidades.Imagen;
import com.edurabroj.donape.shared.entidades.Necesidad;
import com.edurabroj.donape.shared.entidades.Publicacion;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

public class RepoDetallePublicacionGraphql implements RepoDetallePublicacion {
    private ApolloClient client;

    public RepoDetallePublicacionGraphql(ApolloClient client) {
        this.client = client;
    }

    @Override
    public Observable<Publicacion> getPublicacionDetalleData(int id) {
        PublicacionQuery query = PublicacionQuery.builder().id(id).build();
        ApolloCall<PublicacionQuery.Data> apolloCall = client.query(query);

        return Rx2Apollo.from(apolloCall)
                .toObservable()
                .concatMap((Response<PublicacionQuery.Data> dataResponse) ->
                        Observable.just(dataResponse.data().publicacion())
                )
                .concatMap((Function<PublicacionQuery.Publicacion, Observable<Publicacion>>) publicacionApi -> {
                            Publicacion publicacion = obtenerPublicacionMapeada(publicacionApi);
                            return Observable.just(publicacion);
                        }
                );
    }

    private Publicacion obtenerPublicacionMapeada(PublicacionQuery.Publicacion publicacionApi) {
        Publicacion publicacion = new Publicacion();

        publicacion.setId(publicacionApi.id());
        publicacion.setTitulo(publicacionApi.titulo());
        publicacion.setDescripcion(publicacionApi.descripcion());

        List<Imagen> imagenes = obtenerImagenesPublicacion(publicacionApi);
        publicacion.setImagenes(imagenes);

        List<Necesidad> necesidades = obtenerNecesidadesPublicacion(publicacionApi);
        publicacion.setNecesidades(necesidades);

        return publicacion;
    }

    private List<Imagen> obtenerImagenesPublicacion(PublicacionQuery.Publicacion publicacionApi) {
        List<Imagen> imagenes = new ArrayList<>();

        if (publicacionApi.imagenes() != null) {
            for (PublicacionQuery.Imagene imagenApi : Objects.requireNonNull(publicacionApi.imagenes())) {
                Imagen imagen = new Imagen();
                imagen.setUrl(imagenApi.url());

                imagenes.add(imagen);
            }
        }

        return imagenes;
    }

    private List<Necesidad> obtenerNecesidadesPublicacion(PublicacionQuery.Publicacion publicacionApi) {
        List<Necesidad> necesidades = new ArrayList<>();

        if (publicacionApi.necesidades() != null) {
            for (PublicacionQuery.Necesidade necesidadApi : publicacionApi.necesidades()) {
                Necesidad necesidad = new Necesidad();

                necesidad.setId(necesidadApi.id());
                necesidad.setArticulo(necesidadApi.articulo());
                necesidad.setCantidadRequerida(necesidadApi.cantidadRequerida() == null ? 0 : necesidadApi.cantidadRequerida());
                necesidad.setCantidadRecolectada(necesidadApi.cantidadRecolectada() == null ? 0 : necesidadApi.cantidadRecolectada());
                necesidad.setCantidadFaltante(necesidadApi.cantidadFaltante() == null ? 0 : necesidadApi.cantidadFaltante());

                necesidades.add(necesidad);
            }
        }

        return necesidades;
    }
}
