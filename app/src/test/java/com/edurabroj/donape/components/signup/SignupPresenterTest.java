package com.edurabroj.donape.components.signup;

import com.edurabroj.donape.shared.entidades.SignupResponse;
import com.edurabroj.donape.testRules.FakeSchedulerRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import io.reactivex.Observable;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SignupPresenterTest {

    @Rule
    public final FakeSchedulerRule fakeSchedulerRule = new FakeSchedulerRule();

    private SignupPresenter presenter;
    private Signup.Interactor interactor;
    private Signup.View view;

    @Before
    public void setUp() throws Exception {
        interactor = mock(Signup.Interactor.class);
        view = mock(Signup.View.class);

        presenter = new SignupPresenter(interactor);
        presenter.attach(view);

        when(view.getEmail()).thenReturn("email@mail.com");
        when(view.getFullname()).thenReturn("Roberto");
        when(view.getDni()).thenReturn("74523689");
        when(view.getPhone()).thenReturn("951753123");
        when(view.getPassword()).thenReturn("p@ssw0rd");

        Observable<SignupResponse> response = Observable.just(new SignupResponse());
        when(interactor.signUp(any())).thenReturn(response);
    }


    @Test
    public void whenEmailIsEmptyShowsMessage() {
        when(view.getEmail()).thenReturn("");
        presenter.onSignupClick();
        verify(view).showEmailRequired();
    }

    @Test
    public void whenEmailIsInvalidShowsMessage() {
        when(view.getEmail()).thenReturn("edu");
        presenter.onSignupClick();
        verify(view).showEmailInvalid();
    }

    @Test
    public void whenEmailIsValidDoesntShowsMessage() {
        when(view.getEmail()).thenReturn("edu@gmail.com");
        presenter.onSignupClick();
        verify(view, never()).showEmailInvalid();
    }

    @Test
    public void whenFullnameIsEmptyShowsMessage() {
        when(view.getFullname()).thenReturn("");
        presenter.onSignupClick();
        verify(view).showFullnameRequired();
    }

    @Test
    public void whenFullnameIsNotEmptyDoesntShowsMessage() {
        when(view.getFullname()).thenReturn("Eduardo Rabanal");
        presenter.onSignupClick();
        verify(view, never()).showFullnameRequired();
    }

    @Test
    public void whenDniIsEmptyShowsMessage() {
        when(view.getDni()).thenReturn("");
        presenter.onSignupClick();
        verify(view).showDniRequired();
    }

    @Test
    public void whenPhoneIsEmptyShowsMessage() {
        when(view.getPhone()).thenReturn("");
        presenter.onSignupClick();
        verify(view).showPhoneRequired();
    }

    @Test
    public void whenPasswordIsEmptyShowsMessage() {
        when(view.getPassword()).thenReturn("");
        presenter.onSignupClick();
        verify(view).showPasswordRequired();
    }

    @Test
    public void whenPasswordIsNotEmptyDoesntShowsMessage() {
        when(view.getPassword()).thenReturn("hola123");
        presenter.onSignupClick();
        verify(view, never()).showPasswordRequired();
    }

    @Test
    public void whenPasswordIsInvalidShowsMessage() {
        when(view.getPassword()).thenReturn("12");
        presenter.onSignupClick();
        verify(view).showInvalidPassword();
    }

    @Test
    public void whenPasswordIsValidDoesntShowsMessage() {
        when(view.getPassword()).thenReturn("hola1234");
        presenter.onSignupClick();
        verify(view, never()).showInvalidPassword();
    }


    @Test
    public void whenDniIsInvalidShowsMessage() {
        when(view.getDni()).thenReturn("142asdd");
        presenter.onSignupClick();
        verify(view).showInvalidDni();
    }

    @Test
    public void whenDniIsValidDoenstShowsMessage() {
        when(view.getDni()).thenReturn("72850857");
        presenter.onSignupClick();
        verify(view, never()).showInvalidDni();
    }

    @Test
    public void whenPhoneIsInvalidShowsMessage() {
        when(view.getPhone()).thenReturn("361389");
        presenter.onSignupClick();
        verify(view).showInvalidPhone();
    }

    @Test
    public void whenPhoneIsValidDoesntShowsMessage() {
        when(view.getPhone()).thenReturn("976446504");
        presenter.onSignupClick();
        verify(view, never()).showInvalidPhone();
    }

    @Test
    public void whenFormOkDoSignup() {
        when(view.getEmail()).thenReturn("edurabroj@gmail.com");
        when(view.getFullname()).thenReturn("Eduardo Rabanal");
        when(view.getDni()).thenReturn("72850857");
        when(view.getPhone()).thenReturn("934465807");
        when(view.getPassword()).thenReturn("password");
        presenter.onSignupClick();
        verify(interactor).signUp(any());
    }


}