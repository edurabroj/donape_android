package com.edurabroj.donape.components.login;

import com.edurabroj.donape.shared.preferences.IPreferences;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LoginPresenterTest {
    private LoginPresenter loginPresenter;
    private LoginContract.View view;
    private LoginContract.Interactor interactor;
    IPreferences prefs;

    @Before
    public void setUp() throws Exception {
        view = Mockito.mock(LoginContract.View.class);
        interactor = Mockito.mock(LoginContract.Interactor.class);
        prefs = Mockito.mock(IPreferences.class);
        loginPresenter = new LoginPresenter(view, interactor, prefs);
    }

    @Test
    public void usuarioVacioMuestraError() {
        loginPresenter.onLoginButtonClick("", "123");
        verify(view).errorUserRequired();
    }

    @Test
    public void passVacioMuestraError() {
        loginPresenter.onLoginButtonClick("ola", "");
        verify(view).errorPasswordRequired();
    }

    @Test
    public void credencialesCompletasMuestraProgress() {
        loginPresenter.onLoginButtonClick("mimo", "123");
        verify(view).showProgress();
    }

    @Test
    public void credencialesCompletasLlamaGetLoginRespuesta() {
        String user = "mimo", pass = "123";
        loginPresenter.onLoginButtonClick(user, pass);
        verify(interactor).getLoginRespuesta(user, pass, loginPresenter);
    }

    @Test
    public void whenSignupClickedLaunchActivity() {
        loginPresenter.onSignupClick();
        verify(view).launchSignup();
    }

    @Test
    public void whenLoginSavedLaunchesHome(){
        when(prefs.hasLoginSaved()).thenReturn(true);
        loginPresenter.checkSavedLogin();
        verify(view).launchHome();
    }

    @Test
    public void whenNoLoginSavedNoLaunchesHome(){
        when(prefs.hasLoginSaved()).thenReturn(false);
        loginPresenter.checkSavedLogin();
        verify(view,never()).launchHome();
    }
}